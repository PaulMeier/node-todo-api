const env = process.env.NODE_ENV;

const development = {
 app: {
   port: parseInt(process.env.DEV_APP_PORT) || 3000
 },
 db: {
   uri: null,
   host: process.env.DEV_DB_HOST || 'mongo',
   port: parseInt(process.env.DEV_DB_PORT) || 27017,
   name: process.env.DEV_DB_NAME || 'TodoAppTest'
 }
};
const test = {
 app: {
   port: parseInt(process.env.TEST_APP_PORT) || 3000
 },
 db: {
   uri: null,
   host: process.env.TEST_DB_HOST || 'mongo',
   port: parseInt(process.env.TEST_DB_PORT) || 27017,
   name: process.env.TEST_DB_NAME || 'TodoAppTest'
 }
};
const production = {
    app: {
      port: parseInt(process.env.PORT) || parseInt(process.env.PROD_APP_PORT) || 3000
    },
    db: {
      uri: process.env.MONGODB_URI,
      host: process.env.PROD_DB_HOST || 'mongo',
      port: parseInt(process.env.PROD_DB_PORT) || 27017,
      name: process.env.PROD_DB_NAME || 'TodoApp'
    }
};

const config = {
 development,
 test,
 production
};

module.exports = config[env];
