const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://mongodb.spudsbyspud.com:27017/TodoApp', (err, db) => {
    if (err) {
        console.log('Unable to connect to MongoDB server.');
    }
    console.log("Connected to MongoDB server.");

    // db.collection('Todos').find({_id: new ObjectID('5c6850e8ee61b0c5fc1a71f7')}).toArray().then((docs) => {
    //     console.log("Todos");
    //     console.log(JSON.stringify(docs, undefined, 2));
    // }, (err) => {
    //     console.log('Unable to fetch todos.', err);
    // });

    // db.collection('Todos').find().count().then((count) => {
    //     console.log(`Todos count: ${count}` );
    // }, (err) => {
    //     console.log('Unable to fetch todos.', err);
    // });

    db.collection('Users').find({name: 'Paul'}).toArray().then((users) => {
        console.log("Users Named Paul");
        console.log(JSON.stringify(users, undefined, 2));
    }, (err) => {
        console.log('Unable to fetch users.', err);
    });

    // db.close();
});