const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://mongodb.spudsbyspud.com:27017/TodoApp', (err, db) => {
    if (err) {
        console.log('Unable to connect to MongoDB server.');
    }
    console.log("Connected to MongoDB server.");

    // DELETE MANY
    // db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((result) => {
    //     console.log("Result: ", result);
    // });
    // DELETE ONE
    // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
    //     console.log("Result: ", result);
    // });
    // FIND ONE AND DELETE
    // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
    //     console.log("Result: ", result);
    // });
    // REMOVE ANY DUPLICATE USERS
    // db.collection('Users').deleteMany({name: 'Paul'});
    // FIND ONE USER FROM ID AND DELETE
    // db.collection('Users').deleteOne({_id: new ObjectID('5c684c67bbb8f66cff62eae0')}).then((result) => {
    //     console.log(JSON.stringify(result, undefined, 2));
    // });
    // 
    // db.close();
});