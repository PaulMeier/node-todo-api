const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} =  require('./../server/models/todo');
const {User} = require('./../server/models/user');

// var id = '5c687def89e78c0f7e29d8df';

// if(!ObjectID.isValid(id)){
//     console.log("ID not valid.");
// }

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos)
// });

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todo', todo)
// });

// Todo.findById(id).then((todo) => {
//     if (!todo) {
//         console.log('Id not found.')
//     } else {
//         console.log('Todo', todo)
//     }
// }).catch((e) => {console.log(e)});

User.findById('5c687def89e78c0f7e29d8df').then((user) => {
    if (!user) {
        console.log('User not found.')
    }
        console.log(JSON.stringify(user));
    }, (e) => {
        console.log(e);
});