require('dotenv').config();
var config = require('./../../config');
var mongoose = require('mongoose');

const { db: { uri, host, port, name } } = config;
const connectionString = `mongodb://${host}:${port}/${name}`;

mongoose.Promise = global.Promise;
mongoose.connect(uri || connectionString);

module.exports = {mongoose}
